package com.example.demo;

import java.util.List;

import org.identityconnectors.common.security.GuardedString;
import org.identityconnectors.framework.api.APIConfiguration;
import org.identityconnectors.framework.api.ConfigurationProperties;
import org.identityconnectors.framework.api.ConfigurationProperty;
import org.identityconnectors.framework.api.ConnectorFacade;
import org.identityconnectors.framework.api.ConnectorFacadeFactory;
import org.identityconnectors.framework.api.ConnectorInfo;
import org.identityconnectors.framework.api.ConnectorInfoManager;
import org.identityconnectors.framework.api.ConnectorInfoManagerFactory;
import org.identityconnectors.framework.api.ConnectorKey;
import org.identityconnectors.framework.api.RemoteFrameworkConnectionInfo;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoApplication {
	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
		String pass = "partha";

		ConnectorInfoManagerFactory fact = ConnectorInfoManagerFactory.getInstance();
		RemoteFrameworkConnectionInfo remoteFrameworkConnectionInfo = new RemoteFrameworkConnectionInfo("localhost", 8759, new GuardedString(pass.toCharArray()));

		ConnectorInfoManager manager = fact.getRemoteManager(remoteFrameworkConnectionInfo);
		System.out.println(manager);
		ConnectorKey key = new ConnectorKey("net.tirasa.connid.bundles.servicenow", "1.0.2-SNAPSHOT", "net.tirasa.connid.bundles.servicenow.SNConnector");   
		System.out.println(key);
		ConnectorInfo info = manager.findConnectorInfo(key);
		System.out.println(info);

		// From the ConnectorInfo object, create the default APIConfiguration.
		APIConfiguration apiConfig = info.createDefaultAPIConfiguration();
		
		// From the default APIConfiguration, retrieve the ConfigurationProperties.
		ConfigurationProperties properties = apiConfig.getConfigurationProperties();
		
		// Print out what the properties are (not necessary)
		List<String> propertyNames = properties.getPropertyNames();
		//System.out.println(propertyNames.size());
		for(String propName : propertyNames) {
			ConfigurationProperty prop = properties.getProperty(propName);
			System.out.println("Property Name: " + prop.getName() + "\tProperty Type: " + prop.getType() + "\tProperty Value: "+prop.getValue() );
		}

		// Set all of the ConfigurationProperties needed by the connector.
		//properties.setPropertyValue("sampleProperty", "sasas");
		properties.setPropertyValue("username", "john123");
		properties.setPropertyValue("password", new GuardedString("mmm@619".toCharArray()));
		properties.setPropertyValue("baseAddress", "asdasdad");

		// Use the ConnectorFacadeFactory's newInstance() method to get a new connector.
		ConnectorFacade conn = ConnectorFacadeFactory.getInstance().newInstance(apiConfig);

		// Make sure we have set up the Configuration properly
		conn.validate();
		System.out.println("Done");
	}
}
